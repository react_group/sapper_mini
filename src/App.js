import './App.css';
import FieldBox from "./components/fieldBox/fieldBox";
import {useState} from "react";

function App() {

  const createAllFields = () => {
    let allFields = [];
    let y = Math.floor(Math.random() * 36);
    console.log(`Ring is ${y}`)
    for (let i = 0; i < 36; i++) {
      console.log(y)
      if (i === y ) {
        let field = {
          index: i,
          hasItem: true,
          open: false
        }
        allFields.push(field);
      } else {
        let field = {
          index: i,
          hasItem: false,
          open: false
        }
        allFields.push(field);
      }
    }
    return allFields;
  }


  const [fields, setFields] = useState(createAllFields(),
  )


  const openField = (id, open) => {
    console.log(id)
    if (open === false) {
      setFields(fields.map(f => {
        if (f.index === id) {
          console.log(f)
          return {...f, open: true}
        }
        return f;
      }))
    }
  }


  return (
    <div className="App container">
      <FieldBox
          fields = {fields}
          openField = {(id, open) => openField(id, open)}
      >
      </FieldBox>
    </div>
  );
}

export default App;
