import React from 'react';
import './field.css'

const Field = (props) => {

    let fieldClasses = ["field", ]

    if (props.open) {
        fieldClasses.push('open')
    }

    if (props.hasItem) {
        fieldClasses.push('ringHere')
    }


    return (
        <div className={fieldClasses.join(' ')}
        onClick={props.openField}
        >
            {props.index}
        </div>
    );
};

export default Field;